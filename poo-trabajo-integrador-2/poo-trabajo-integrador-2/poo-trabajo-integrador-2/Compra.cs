﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:03 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of Compra.
	/// </summary>
	public class Compra
	{
		private List<Producto> chango;
		private Cliente comprador;
		private Tarjeta plastico;
		private KeyValuePair<uint,double> formaPago;
		
		//Constructor vacío únicamente
		public Compra()
		{
			chango = new List<Producto>();
			formaPago = new KeyValuePair<uint, double>(1,0);
		}
		
		//Propiedades de Lázaro Báez
		public Cliente Comprador{
			get{return this.comprador;}
			set{this.comprador=value;}
		}
		public Tarjeta Plastico{
			get{return this.plastico;}
			set{this.plastico=value;}
		}
		public List<Producto> Chango{
			get{return this.chango;}//ojo que devuelve refs originales
			set{this.chango=value;}
		}
		public KeyValuePair<uint,double> FormaPago{
			get{return this.formaPago;}
			set{formaPago = value;}
		}
		//Métodos
		public void AgregarProducto(Producto p)
		{
			chango.Add(p);
		}
		public bool QuitarProducto(Producto p)
		{
			bool success=false;
			if (chango.Contains(p)) {
				success=chango.Remove(p);
			}
			
			return success;
		}
		public double CalcularPrecio()
		{
			double total=0;
			foreach (Producto p in this.chango) {
				total=total+p.Precio;
			}
			return total;
		}
		public double CalcularPrecioConInteres()
		{
			double total = this.CalcularPrecio()*(1+this.formaPago.Value/100);
			return total;
		}
		
		public void Limpiar()
		{
			this.comprador=null;
			this.plastico=null;
			chango.Clear();
		}
	}
}
