﻿/*
 * Created by SharpDevelop.
 * User: guille puertas
 * Date: 21/11/2018
 * Time: 02:25 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		Tienda negocio = new Tienda("7 en 90");
		CuotasForm forma = new CuotasForm();
		FPForm formaFP;
		
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
			// AGREGO ALGUNOS PRODUCTOS, CLIENTES Y TARJETAS DE EJEMPLO
			negocio.productos.Agregar(new Producto("zapatilla","topper","40",2000,0));
			negocio.productos.Agregar(new Producto("zapatilla","adidas","41",4000,0));
			negocio.productos.Agregar(new Producto("pelota","caprichito","n.3",100,10));
			negocio.productos.Agregar(new Producto("pelota","dunlop","n.5",1500,0));
			
			negocio.clientes.Agregar( new Cliente("Mauricio","Huberman",12345678, new DateTime(1983, 5, 1, 0, 0, 0)));
			negocio.clientes.Agregar( new Cliente("Jorge","Sampalobby",23456789, new DateTime(1960, 12, 2, 0, 0, 0)));
			negocio.clientes.Agregar( new Cliente("Rogelio","Aguas",34567890, new DateTime(2008, 3, 30, 0, 0, 0)));
			
			Tarjeta visaF = new Tarjeta("Visa","Francés");
			visaF.AgregarFP(3,10);
			visaF.AgregarFP(6,20);
			visaF.AgregarFP(9,30);

			negocio.tarjetas.Agregar(visaF);
			
			Tarjeta nativa = new Tarjeta("Nativa","Nación");
			nativa.AgregarFP(3,15);
			nativa.AgregarFP(6,25);
			nativa.AgregarFP(9,30);
			negocio.tarjetas.Agregar(nativa);
			
			negocio.tarjetas.Agregar(new Tarjeta("Master","Nación"));
			
			// COSAS DEL FORM
			this.Text = negocio.nombre+" artículos deportivos";
			tabPage1.Text="Productos y Promociones";
			tabPage2.Text="Productos y Promociones b";
			tabPage3.Text="Compras";
			tabPage4.Text="Tarjetas de Crédito";
			tabPage5.Text="Administración";
			
			// PRODUCTOS Y PROMOCIONES
			groupBox1.Text = "Administre productos";
			checkBox1.Text="Mostrar solo productos con promoción";

			groupBox2.Text = "Agregar producto";
			button1.Text = "Agregar";
			label1.Text="Tipo";
			label2.Text="Marca";
			label3.Text="Talle";
			label4.Text="Precio";
			label5.Text="Promo";
			
			groupBox3.Text = "Promo";
			button2.Text = "Agregar Promo";
			label6.Text = "Promo";
			
			listView1.Columns.Add("Tipo", -2, HorizontalAlignment.Right);
			listView1.Columns.Add("Marca", -2, HorizontalAlignment.Right);
			listView1.Columns.Add("Talle", -2, HorizontalAlignment.Right);
			listView1.Columns.Add("Precio", -2, HorizontalAlignment.Right);
			listView1.Columns.Add("Descuento", -2, HorizontalAlignment.Right);
			listView1.CheckBoxes=true;
			listView1.View = View.Details;
			ListarProductos(listView1, negocio.productos.Listar());

			dataGridView1.AllowUserToDeleteRows=false;
			dataGridView1.ColumnCount = 5;
			dataGridView1.Columns[0].Name = "Tipo";
			dataGridView1.Columns[1].Name = "Marca";
			dataGridView1.Columns[2].Name = "Talle";
			dataGridView1.Columns[3].Name = "Precio";
			dataGridView1.Columns[4].Name = "Promo";
			
			ListarProductosDGV(negocio.productos.Listar());
			
			checkBox2.Text="Mostrar solo productos con promoción";
			
			// COMPRAS
			listView2.Columns.Add("Tipo", -2, HorizontalAlignment.Right);
			listView2.Columns.Add("Marca", -2, HorizontalAlignment.Right);
			listView2.Columns.Add("Talle", -2, HorizontalAlignment.Right);
			listView2.Columns.Add("Precio", -2, HorizontalAlignment.Right);
			listView2.Columns.Add("Descuento", -2, HorizontalAlignment.Right);
			listView2.View = View.Details;
			ListarProductos(listView2, negocio.productos.Listar());
			
			listView3.Columns.Add("Tipo", 80, HorizontalAlignment.Right);
			listView3.Columns.Add("Marca", -2, HorizontalAlignment.Right);
			listView3.Columns.Add("Talle", -2, HorizontalAlignment.Right);
			listView3.Columns.Add("Precio", -2, HorizontalAlignment.Right);
			listView3.Columns.Add("Descuento", -2, HorizontalAlignment.Right);
			listView3.View = View.Details;
			//listView3.CheckBoxes=true;
			ListarProductos(listView3, negocio.compraActual.Chango);
			
			listView4.HideSelection=false;
			
			button3.Text = "Agregar";
			button4.Text = "Quitar";
			button9.Text = "Nueva Compra";
			label7.Text = "Seleccione DNI del cliente";
			label8.Text = "Agregue cliente nuevo";
			label9.Text="Nombre";
			label10.Text="Apellido";
			label11.Text="DNI";
			label12.Text="Fecha de nac. (DD/MM/AA)";
			ListarClientesCB(comboBox1,negocio.clientes.Listar());
			button5.Text="Agregar";
			button6.Text="Comprar";
			
			ListarTarjetasLV(listView4,negocio.tarjetas.Listar());
			listView4.ShowGroups=true;
			label13.Text="Aún no seleccionó cliente";
			label14.Text="Aún no seleccionó tarjeta";
			label15.Text = "Total carro $"+ negocio.compraActual.CalcularPrecio().ToString();
			label16.Text = "Total con interés $"+ negocio.compraActual.CalcularPrecioConInteres().ToString();
			
			groupBox4.Text = " Seleccione cliente";
			groupBox5.Text = " Seleccione tarjeta";
			
			// TARJETAS
			listView5.ShowGroups=true;
			ListarTarjetasLV(listView5,negocio.tarjetas.Listar());
			button7.Text = "Agregar!";
			button8.Text = "Agregar!";
			label17.Text = "Agregar Tarjetas";
			label18.Text = "Agregar Formas de Pago.\r\n Indique cantidad de formas de pago a agregar.";
			//label18.AutoSize = true;
			groupBox6.Text = "Operaciones";
			
			checkBox3.Text = "Mostrar solamente tarjetas con beneficio";
			label23.Text = "Agregar beneficio (seleccione forma de pago e ingrese porcentaje)";
			button10.Text = "Agregar beneficio";
			
			//ADMINISTRACION
			label19.Text = "Total vendido en la tienda:";
			label20.Text = "$"+negocio.CalcularTotalCompras().ToString();
			label21.Text = "Total vendido por cliente:";
			label22.Text = "Total vendido por tarjeta:";
			
			listView6.Columns.Add("DNI", -2, HorizontalAlignment.Right);
			listView6.Columns.Add("Nombre", -2, HorizontalAlignment.Right);
			listView6.Columns.Add("Total gastado", -2, HorizontalAlignment.Right);
			listView6.View = View.Details;
			ListarClientesLV(listView6, negocio.clientes.Listar());
			
			listView7.Columns.Add("Tarjeta", -2, HorizontalAlignment.Right);
			listView7.Columns.Add("Total gastado", -2, HorizontalAlignment.Right);
			listView7.View = View.Details;
			ListarTarjetasLV(listView7, negocio.tarjetas.Listar(),true);
			
//			listView1.HideSelection=false;
//			listView2.HideSelection=false;
//			listView3.HideSelection=false;
//			listView4.HideSelection=false;
			listView5.HideSelection=false;
		}
		
		// LISTAR PRODUCTOS EN UN LISTVIEW
		void ListarProductos(ListView vista, List<Producto> lista)
		{
			vista.Items.Clear();
			foreach (Producto p in lista) {
				ListViewItem itemNuevo = new ListViewItem(p.Tipo,0);
				itemNuevo.SubItems.Add(p.Marca);
				itemNuevo.SubItems.Add(p.Talle);
				itemNuevo.SubItems.Add("$"+p.Precio.ToString());
				itemNuevo.SubItems.Add(p.Promo.ToString()+"%");
				vista.Items.Add(itemNuevo);
			}
		}
		
		// LISTAR CLIENTES EN COMBOBOX
		void ListarClientesCB(ComboBox cb, List<Cliente> lista)
		{
			cb.Items.Clear();
			foreach (Cliente c in lista) {
				cb.Items.Add(c.Dni);
			}
		}
		
		// LISTAR CLIENTES EN LISTVIEW
		void ListarClientesLV(ListView lv, List<Cliente> lista)
		{
			lv.Items.Clear();
			foreach (Cliente c in lista) {
				ListViewItem itemNuevo = new ListViewItem(c.Dni.ToString(),0);
				itemNuevo.SubItems.Add(c.NombreCompleto);
				itemNuevo.SubItems.Add("$"+negocio.CalcularTotalComprasPorCliente(c).ToString());
				lv.Items.Add(itemNuevo);
			}
		}
		
		// LISTAR TARJETAS EN LISTBOX
		void ListarTarjetasLB(ListBox lb, List<Tarjeta> lista)
		{
			foreach (Tarjeta t in lista) {
				lb.Items.Add(t.Nombre+t.Banco);
				
			}
		}
		
		// LISTAR TARJETAS EN LISTVIEW
		void ListarTarjetasLV(ListView lv, List<Tarjeta> lista)
		{
			lv.Items.Clear();
			foreach (Tarjeta t in lista) {
				ListViewGroup grupo = new ListViewGroup(t.Nombre.ToString()+"-"+ t.Banco.ToString(),
				                                        HorizontalAlignment.Left);
				lv.Groups.Add(grupo);
				foreach (KeyValuePair<uint,double> kv in t.FormaPago) {
					ListViewItem itemNuevo = new ListViewItem(kv.Key.ToString()+"("+kv.Value.ToString()+"%)",0);
					itemNuevo.Group=grupo;
					lv.Items.Add(itemNuevo);
				}
			}
		}
		
		void ListarTarjetasLV(ListView lv, List<Tarjeta> lista, bool unBooleano)
		{
			lv.Items.Clear();
			foreach (Tarjeta t in lista) {
				ListViewItem itemNuevo = new ListViewItem(t.Nombre.ToString()+" "+t.Banco.ToString(),0);
				itemNuevo.SubItems.Add("$"+negocio.CalcularTotalComprasPorTarjeta(t).ToString());
				lv.Items.Add(itemNuevo);
			}
		}
		
		// LISTAR PRODUCTOS EN DATAGRIDVIEW
		void ListarProductosDGV(List<Producto> lista)
		{
			dataGridView1.Rows.Clear();
			int j=0;
			foreach (Producto p in lista) {
				string[] fila = {p.Tipo, p.Marca, p.Talle, p.Precio.ToString(), p.Promo.ToString()};
				dataGridView1.Rows.Add(fila);
				for (int i = 0; i < 4; i++) {
					dataGridView1.Rows[j].Cells[i].ReadOnly=true;
				}
				j++;
			}
		}
		
		// COMPRA NUEVA, LIMPIA TODO
		void nuevaCompra()
		{
			negocio.compraActual=new Compra();
			ListarProductos(listView3, negocio.compraActual.Chango);
			label13.Text="";
			comboBox1.SelectedIndex = -1;
			label14.Text= "";
			label15.Text = "Total carro $"+ negocio.compraActual.CalcularPrecio().ToString();
			label16.Text = "Total con interés $"+ negocio.compraActual.CalcularPrecioConInteres().ToString();
			foreach (ListViewItem i in listView4.SelectedItems) {
				i.Selected=false;
			}
		}
		
		void TabPage1Click(object sender, EventArgs e)
		{
			
		}
		void CheckBox1CheckedChanged(object sender, EventArgs e)
		{
			if (checkBox1.Checked) {
				ListarProductos(listView1, negocio.productos.ListarPromo());
			}else{
				ListarProductos(listView1, negocio.productos.Listar());
			}
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			try {
				string tipo = textBox1.Text;
				string marca = textBox2.Text;
				string talle = textBox3.Text;
				double precio = double.Parse(textBox4.Text);
				double promo = double.Parse(textBox5.Text);
				if (!(textBox1.Text.Length>0) || !(textBox2.Text.Length>0) || !(textBox3.Text.Length>0) || !(textBox4.Text.Length>0) || !(textBox5.Text.Length>0)) {
					throw new FormatException();
				}else{
					if (precio<0) {
						textBox4.Clear();
						throw new FormatException();
					}
					if (promo<0 || promo>100) {
						textBox5.Clear();
						throw new FormatException();
					}
					Producto nuevoProducto = new Producto(tipo, marca, talle, precio, promo);
					negocio.productos.Agregar(nuevoProducto);
					ListarProductos(listView1, negocio.productos.Listar());
					textBox1.Clear();
					textBox2.Clear();
					textBox3.Clear();
					textBox4.Clear();
					textBox5.Clear();
				}
				
			} catch (FormatException) {
				MessageBox.Show("La información ingresada no es correcta. Se deben completar todos los campos y tanto el precio como la promoción deben ser números positivos.", "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		

		void Button2Click(object sender, EventArgs e)
		{
			try {
				double promo = double.Parse(textBox6.Text);
				//ListView.SelectedListViewItemCollection elegidos = listView1.SelectedItems;
				ListView.CheckedListViewItemCollection elegidos = listView1.CheckedItems;
				foreach (ListViewItem it in elegidos) {
					string tipo = it.Text;
					string marca = it.SubItems[1].Text;
					string talle = it.SubItems[2].Text;
					double precio = double.Parse(it.SubItems[3].Text.Trim('$'));
					double oldPromo = double.Parse(it.SubItems[4].Text.Trim('%'));
					negocio.productos.AgregarPromo(new Producto(tipo, marca, talle, precio, oldPromo), promo);
					ListarProductos(listView1, negocio.productos.Listar());
					textBox6.Clear();
				}
			} catch (FormatException) {
				MessageBox.Show("La información ingresada no es correcta. La promoción deben ser un número positivo.", "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
			}catch(ArgumentOutOfRangeException){
				MessageBox.Show("La información ingresada no es correcta. La promoción deben ser un número positivo.", "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
				textBox6.Clear();
			}
		}
		
		void DataGridView1CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			bool filaLlena;
			DataGridViewRow fila= dataGridView1.Rows[e.RowIndex];
			filaLlena = fila.Cells[0].Value!=null && fila.Cells[1].Value!=null && fila.Cells[2].Value!=null &&
				fila.Cells[3].Value!=null && fila.Cells[4].Value!=null;

			double result;
			try {
				if (e.ColumnIndex==3 && !double.TryParse(fila.Cells[3].Value.ToString(), out result)) {
					throw new FormatException("El precio tiene que ser un número positivo");
				}
				if (e.ColumnIndex==4 && !double.TryParse(fila.Cells[4].Value.ToString(), out result)) {
					throw new FormatException("La promo tiene que ser un número positivo");
				}
				if (filaLlena) {
					
					string tipo= dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
					string marca= dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
					string talle= dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
					double precio= double.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
					double promo= double.Parse(dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString());
					if (negocio.productos.Listar().Exists(x => x.Tipo == tipo && x.Marca == marca && x.Talle == talle)) {
						Producto p=negocio.productos.Listar().Find(x => x.Tipo == tipo && x.Marca == marca && x.Talle == talle);
						negocio.productos.AgregarPromo(p,promo);
					}else{
						negocio.productos.Agregar(new Producto(tipo, marca, talle, precio,promo));
					}
					//ListarProductosDGV(negocio.productos.Listar());
				}
			} catch (FormatException exc) {
				MessageBox.Show("La información ingresada no es correcta. "+exc.Message, "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
				dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value=0;
			}catch(ArgumentOutOfRangeException){
				MessageBox.Show("La información ingresada no es correcta. ", "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
				dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value=0;
			}
		}
		
		void CheckBox2CheckedChanged(object sender, EventArgs e)
		{
			if (checkBox2.Checked) {
				ListarProductosDGV(negocio.productos.ListarPromo());
			}else{
				ListarProductosDGV(negocio.productos.Listar());
			}
		}
		
		void Button3Click(object sender, EventArgs e)
		{
			ListView.SelectedListViewItemCollection elegidos = listView2.SelectedItems;
			foreach (ListViewItem it in elegidos) {
				string tipo = it.Text;
				string marca = it.SubItems[1].Text;
				string talle = it.SubItems[2].Text;
				negocio.compraActual.AgregarProducto(negocio.productos.Listar().Find(x => x.Tipo == tipo && x.Marca == marca && x.Talle == talle));
				ListarProductos(listView3, negocio.compraActual.Chango);
				label15.Text = "Total carro $"+ negocio.compraActual.CalcularPrecio().ToString();
				if (listView4.SelectedItems.Count>0) {
					label16.Text = "Total con interés $"+ negocio.compraActual.CalcularPrecioConInteres().ToString();
				}
			}
			
		}
		void Button4Click(object sender, EventArgs e)
		{
			ListView.SelectedListViewItemCollection elegidos = listView3.SelectedItems;
			//ListView.CheckedListViewItemCollection elegidos = listView3.CheckedItems;
			foreach (ListViewItem it in elegidos) {
				string tipo = it.Text;
				string marca = it.SubItems[1].Text;
				string talle = it.SubItems[2].Text;
				//negocio.compraActual.QuitarProducto(negocio.productos.Listar().Find(x => x.Tipo == tipo && x.Marca == marca && x.Talle == talle));
				Producto p = negocio.compraActual.Chango.Find(x => x.Tipo == tipo && x.Marca == marca && x.Talle == talle);
				negocio.compraActual.QuitarProducto(p);
				ListarProductos(listView3, negocio.compraActual.Chango);
			}
			label15.Text = "Total carro $"+ negocio.compraActual.CalcularPrecio().ToString();
			if (listView4.SelectedItems.Count>0) {
				label16.Text = "Total con interés $"+ negocio.compraActual.CalcularPrecioConInteres().ToString();
			}
		}
		
		void Button5Click(object sender, EventArgs e)
		{
			try {
				string nombre = textBox7.Text;
				string apellido = textBox8.Text;
				uint dni;// = uint.Parse(textBox9.Text);
				if (!uint.TryParse(textBox9.Text, out dni)) {
					throw new FormatException("DNI no válido");
				}
				string strFecha=textBox10.Text;
				string[] arrFecha = strFecha.Split('/');
				DateTime fecha = new DateTime(int.Parse(arrFecha[2]),int.Parse(arrFecha[1]),int.Parse(arrFecha[0]), 8, 30, 52);
				Cliente comprador = new Cliente(nombre, apellido, dni, fecha);
				negocio.clientes.Agregar(comprador);
				negocio.compraActual.Comprador=comprador;
				ListarClientesCB(comboBox1,negocio.clientes.Listar());
				label13.Text=negocio.compraActual.Comprador.NombreCompleto.ToString()+"("+
					negocio.compraActual.Comprador.Dni+")";
			} catch (FormatException exc) {
				MessageBox.Show(exc.Message, "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
			}catch(ArgumentOutOfRangeException){
				MessageBox.Show("Fecha no válida", "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
			}catch(IndexOutOfRangeException){
				MessageBox.Show("Fecha no válida", "Error en la carga de datos",
				                MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		void ComboBox1SelectionChangeCommitted(object sender, EventArgs e)
		{
			uint dni = (uint)comboBox1.SelectedItem;
			Cliente elegido = negocio.clientes.IdentificarCliente(dni);
			negocio.compraActual.Comprador=elegido;
			label13.Text=negocio.compraActual.Comprador.NombreCompleto.ToString()+"("+
				negocio.compraActual.Comprador.Dni+")";
		}
		
		void ListView4ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
		{
			if ( listView4.SelectedItems.Count>0) {
				string tarjeta = listView4.SelectedItems[0].Group.Header;
				string nombre= tarjeta.Split('-')[0];
				string banco= tarjeta.Split('-')[1];
				negocio.compraActual.Plastico = negocio.tarjetas.Listar().Find(x => x.Nombre == nombre && x.Banco == banco);
				
				string fp = listView4.SelectedItems[0].Text;
				string[] fpA = fp.Split('(',')','%');
				uint cuotas = uint.Parse(fpA[0]);
				double perc = double.Parse(fpA[1]);
				negocio.compraActual.FormaPago= new KeyValuePair<uint, double>(cuotas,perc);
				
				label14.Text= tarjeta+"-"+cuotas.ToString()+ " cuotas ("+ perc.ToString()+ "%)";
				
				label15.Text = "Total carro $"+ negocio.compraActual.CalcularPrecio().ToString();
				label16.Text = "Total con interés $"+ negocio.compraActual.CalcularPrecioConInteres().ToString();
			}
			
		}
		
		void Button6Click(object sender, EventArgs e)
		{
			bool hayTarjeta = listView4.SelectedIndices.Count>0;
			bool hayCliente = comboBox1.SelectedItem!=null;
			bool hayProductos = listView3.Items.Count>0;
			if (hayTarjeta && hayCliente && hayProductos) {
				negocio.compras.Agregar(negocio.compraActual);
				MessageBox.Show( "Compra realizada","Todo bien",MessageBoxButtons.OK,MessageBoxIcon.Information);
				//negocio.compraActual.Limpiar();
				nuevaCompra();
			}else{
				MessageBox.Show( "Debe completar todos los campos señor","Te pensaste que me ibas a agarrar?",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}
		
		void Button7Click(object sender, EventArgs e)
		{
			//CuotasForm forma = new CuotasForm();
			var resultado = forma.ShowDialog();
			if (resultado==DialogResult.OK) {
				Tarjeta tarjetaNueva=new Tarjeta(forma.Nombre,forma.Banco);
				if (forma.Fp>1) {
					tarjetaNueva.AgregarFP(1, 0);
					for (int i = 0; i < forma.Fp; i++) {
						formaFP = new FPForm();
						var resultadoFP = formaFP.ShowDialog();
						if (resultado==DialogResult.OK) {
							tarjetaNueva.AgregarFP(formaFP.Cuotas, formaFP.Perc);
						}
					}
				}
				negocio.tarjetas.Agregar(tarjetaNueva);
			}
			ListarTarjetasLV(listView5,negocio.tarjetas.Listar());
		}
		
		void Button8Click(object sender, EventArgs e)
		{
			try {
				int fp;
				string tarjeta = listView5.SelectedItems[0].Group.Header;
				string nombre= tarjeta.Split('-')[0];
				string banco= tarjeta.Split('-')[1];
				
				bool correcto = int.TryParse(textBox11.Text, out fp);
				if (!correcto || fp<1) {
					MessageBox.Show( "Cantidad de formas de pago inválida","Errar es humano",MessageBoxButtons.OK,MessageBoxIcon.Error);
					textBox11.Text="";
				}else{
					if (fp>=1) {
						for (int i=0; i < fp; i++) {
							formaFP = new FPForm();
							
							var resultadoFP = formaFP.ShowDialog();
							formaFP.DesktopLocation = new Point(0,0);
							//formaFP.Location = this.Location;
							//formaFP.SetDesktopLocation(this.Location.X,this.Location.Y);
							if (resultadoFP==DialogResult.OK) {
								negocio.tarjetas.Listar().Find(x => x.Nombre == nombre && x.Banco == banco).AgregarBeneficio(formaFP.Cuotas,formaFP.Perc);
								ListarTarjetasLV(listView5,negocio.tarjetas.Listar());
							}
						}
					}
				}
			} catch (ArgumentOutOfRangeException) {
				MessageBox.Show( "No seleccionó ninguna tarjeta","Erroooooor",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
			
		}
		
		void TabControl1SelectedIndexChanged(object sender, EventArgs e)
		{
			if (tabControl1.SelectedIndex == 0) {
				ListarProductos(listView1, negocio.productos.Listar());
			}
			if (tabControl1.SelectedIndex == 1) {
				ListarProductosDGV(negocio.productos.Listar());
			}
			if (tabControl1.SelectedIndex == 4) {
				label20.Text = "$"+negocio.CalcularTotalCompras().ToString();
				ListarClientesLV(listView6, negocio.clientes.Listar());
				ListarTarjetasLV(listView7, negocio.tarjetas.Listar(),true);
			}
		}
		
		void Button9Click(object sender, EventArgs e)
		{
			nuevaCompra();
		}
		void CheckBox3CheckedChanged(object sender, EventArgs e)
		{
			if (checkBox3.Checked) {
				ListarTarjetasLV(listView5,negocio.tarjetas.Listar().FindAll(x =>x.Beneficio==true));
			}else{
				ListarTarjetasLV(listView5,negocio.tarjetas.Listar());
			}

		}
		
		void Button10Click(object sender, EventArgs e)
		{
			try {
				int fp;
				string tarjeta = listView5.SelectedItems[0].Group.Header;
				string nombre= tarjeta.Split('-')[0];
				string banco= tarjeta.Split('-')[1];

				string fpSeleccionada = listView5.SelectedItems[0].Text;
				string[] fpA = fpSeleccionada.Split('(',')','%');
				uint cuotas = uint.Parse(fpA[0]);
				double perc = double.Parse(fpA[1]);
				//negocio.compraActual.FormaPago= new KeyValuePair<uint, double>(cuotas,perc);
				
				double percNuevo;
				bool correcto = double.TryParse(textBox12.Text, out percNuevo);
				if (!correcto) {
					MessageBox.Show( "Porcentaje inválido","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
					textBox11.Text="";
				}else{
					negocio.tarjetas.Listar().Find(x => x.Nombre == nombre && x.Banco == banco).AgregarBeneficio(cuotas,percNuevo);
					ListarTarjetasLV(listView5,negocio.tarjetas.Listar());
				}
			} catch (ArgumentOutOfRangeException) {
				MessageBox.Show( "No seleccionó ninguna tarjeta","Erroooooor",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}
		void ComboBox1SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
		void ListView4SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}
	}
}
