﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:07 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of Tienda.
	/// </summary>
	public class Tienda
	{
		public ListadoProductos productos= new ListadoProductos();
		public ListadoClientes clientes= new ListadoClientes();
		public ListadoTarjetas tarjetas= new ListadoTarjetas();
		public ListadoCompras compras = new ListadoCompras();
		
		public Compra compraActual = new Compra();
		
		public string nombre;
		
		public Tienda()
		{
			
		}
		
		public Tienda(string nombre)
		{
			this.nombre=nombre;
		}
		
		public double CalcularTotalCompras()
		{
			double total=0;
			foreach (Compra c in compras.Listar()) {
				total = total+c.CalcularPrecioConInteres();
			}
			return total;
		}
		
		public double CalcularTotalComprasPorCliente(Cliente clientirijillo)
		{
			List<Compra> comprasCliente = compras.Listar().FindAll(x => x.Comprador.Dni==clientirijillo.Dni);
			double total=0;
			foreach (Compra c in comprasCliente) {
				total = total+c.CalcularPrecioConInteres();
			}
			return total;
		}
		
		public double CalcularTotalComprasPorTarjeta(Tarjeta tarjetirijilla)
		{
			List<Compra> comprasTarjeta = compras.Listar().FindAll(x => x.Plastico.Nombre==tarjetirijilla.Nombre && x.Plastico.Banco==tarjetirijilla.Banco);
			double total=0;
			foreach (Compra c in comprasTarjeta) {
				total = total+c.CalcularPrecioConInteres();
			}
			return total;
		}
	}
}
