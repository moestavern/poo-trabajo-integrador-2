﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:02 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of Cliente.
	/// </summary>
	public class Cliente
	{
		private string nombre;
		private string apellido;
		private uint dni;
		private DateTime fechaNac;
		
		//Constructores
		public Cliente()
		{
		}
		public Cliente(string nombre, string apellido, uint dni, DateTime fechaNac)
		{
			this.nombre=nombre;
			this.apellido=apellido;
			this.dni=dni;
			this.fechaNac=fechaNac;
		}
		
		//Propiedades
		public string Nombre{get{return this.nombre;}}
		public string Apellido{get{return this.apellido;}}
		public uint Dni{get{return this.dni;}}
		public DateTime FechaNac{get{return this.fechaNac;}}
		public string NombreCompleto{get{return this.apellido+", "+this.nombre;}}
	}
}
