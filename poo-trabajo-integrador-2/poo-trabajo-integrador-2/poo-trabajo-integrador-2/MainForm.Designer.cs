﻿/*
 * Created by SharpDevelop.
 * User: guille puertas
 * Date: 21/11/2018
 * Time: 02:25 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace poo_trabajo_integrador_2
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.ListView listView3;
		private System.Windows.Forms.ListView listView2;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBox10;
		private System.Windows.Forms.TextBox textBox9;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.ListView listView4;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.ListView listView5;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.ListView listView6;
		private System.Windows.Forms.ListView listView7;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.Label label23;
		/*private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.CheckBox checkBox3;*/
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		private void InitializeComponent()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label6 = new System.Windows.Forms.Label();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.listView1 = new System.Windows.Forms.ListView();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.checkBox2 = new System.Windows.Forms.CheckBox();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.button9 = new System.Windows.Forms.Button();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label14 = new System.Windows.Forms.Label();
			this.listView4 = new System.Windows.Forms.ListView();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.label13 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.label7 = new System.Windows.Forms.Label();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.listView3 = new System.Windows.Forms.ListView();
			this.listView2 = new System.Windows.Forms.ListView();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.button10 = new System.Windows.Forms.Button();
			this.textBox12 = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.checkBox3 = new System.Windows.Forms.CheckBox();
			this.textBox11 = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.listView5 = new System.Windows.Forms.ListView();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.label19 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.listView6 = new System.Windows.Forms.ListView();
			this.label22 = new System.Windows.Forms.Label();
			this.listView7 = new System.Windows.Forms.ListView();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.tabPage3.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.flowLayoutPanel3.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(753, 559);
			this.tabControl1.TabIndex = 0;
			this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.TabControl1SelectedIndexChanged);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.flowLayoutPanel2);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(745, 533);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Controls.Add(this.groupBox1);
			this.flowLayoutPanel2.Controls.Add(this.listView1);
			this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(739, 527);
			this.flowLayoutPanel2.TabIndex = 3;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.checkBox1);
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(389, 519);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "groupBox1";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.textBox6);
			this.groupBox3.Controls.Add(this.button2);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.Location = new System.Drawing.Point(3, 224);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(383, 169);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "groupBox3";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(182, 19);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(52, 23);
			this.label6.TabIndex = 2;
			this.label6.Text = "label6";
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(240, 19);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(129, 20);
			this.textBox6.TabIndex = 1;
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(6, 19);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(102, 23);
			this.button2.TabIndex = 0;
			this.button2.Text = "button2";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.Button2Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.textBox5);
			this.groupBox2.Controls.Add(this.textBox4);
			this.groupBox2.Controls.Add(this.textBox3);
			this.groupBox2.Controls.Add(this.textBox2);
			this.groupBox2.Controls.Add(this.textBox1);
			this.groupBox2.Controls.Add(this.button1);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox2.Location = new System.Drawing.Point(3, 40);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(383, 184);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "groupBox2";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(182, 123);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(52, 23);
			this.label5.TabIndex = 10;
			this.label5.Text = "label5";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(182, 97);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 23);
			this.label4.TabIndex = 9;
			this.label4.Text = "label4";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(182, 71);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 23);
			this.label3.TabIndex = 8;
			this.label3.Text = "label3";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(182, 45);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(52, 23);
			this.label2.TabIndex = 7;
			this.label2.Text = "label2";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(182, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(52, 23);
			this.label1.TabIndex = 6;
			this.label1.Text = "label1";
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(240, 123);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(129, 20);
			this.textBox5.TabIndex = 5;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(240, 97);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(129, 20);
			this.textBox4.TabIndex = 4;
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(240, 71);
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(129, 20);
			this.textBox3.TabIndex = 3;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(240, 45);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(129, 20);
			this.textBox2.TabIndex = 2;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(240, 19);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(129, 20);
			this.textBox1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(6, 19);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(102, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// checkBox1
			// 
			this.checkBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.checkBox1.Location = new System.Drawing.Point(3, 16);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(383, 24);
			this.checkBox1.TabIndex = 0;
			this.checkBox1.Text = "checkBox1";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckBox1CheckedChanged);
			// 
			// listView1
			// 
			this.listView1.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.listView1.Location = new System.Drawing.Point(398, 3);
			this.listView1.Name = "listView1";
			this.listView1.Size = new System.Drawing.Size(336, 519);
			this.listView1.TabIndex = 0;
			this.listView1.UseCompatibleStateImageBehavior = false;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.checkBox2);
			this.tabPage2.Controls.Add(this.dataGridView1);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(745, 533);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// checkBox2
			// 
			this.checkBox2.Location = new System.Drawing.Point(8, 441);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(224, 24);
			this.checkBox2.TabIndex = 1;
			this.checkBox2.Text = "checkBox2";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox2.CheckedChanged += new System.EventHandler(this.CheckBox2CheckedChanged);
			// 
			// dataGridView1
			// 
			this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Top;
			this.dataGridView1.Location = new System.Drawing.Point(3, 3);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(739, 432);
			this.dataGridView1.TabIndex = 0;
			this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellValueChanged);
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.button9);
			this.tabPage3.Controls.Add(this.label16);
			this.tabPage3.Controls.Add(this.label15);
			this.tabPage3.Controls.Add(this.groupBox5);
			this.tabPage3.Controls.Add(this.groupBox4);
			this.tabPage3.Controls.Add(this.button6);
			this.tabPage3.Controls.Add(this.button4);
			this.tabPage3.Controls.Add(this.button3);
			this.tabPage3.Controls.Add(this.listView3);
			this.tabPage3.Controls.Add(this.listView2);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(745, 533);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(335, 103);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(66, 44);
			this.button9.TabIndex = 22;
			this.button9.Text = "button9";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.Button9Click);
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(8, 470);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(547, 23);
			this.label16.TabIndex = 21;
			this.label16.Text = "label16";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(8, 451);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(547, 23);
			this.label15.TabIndex = 20;
			this.label15.Text = "label15";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.label14);
			this.groupBox5.Controls.Add(this.listView4);
			this.groupBox5.Location = new System.Drawing.Point(407, 176);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(326, 259);
			this.groupBox5.TabIndex = 19;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "groupBox5";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(6, 216);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(299, 23);
			this.label14.TabIndex = 17;
			this.label14.Text = "label14";
			// 
			// listView4
			// 
			this.listView4.Location = new System.Drawing.Point(6, 18);
			this.listView4.Name = "listView4";
			this.listView4.Size = new System.Drawing.Size(314, 177);
			this.listView4.TabIndex = 0;
			this.listView4.UseCompatibleStateImageBehavior = false;
			this.listView4.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.ListView4ItemSelectionChanged);
			this.listView4.SelectedIndexChanged += new System.EventHandler(this.ListView4SelectedIndexChanged);
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.label13);
			this.groupBox4.Controls.Add(this.tableLayoutPanel1);
			this.groupBox4.Location = new System.Drawing.Point(8, 176);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(345, 259);
			this.groupBox4.TabIndex = 18;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "groupBox4";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(6, 216);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(299, 23);
			this.label13.TabIndex = 16;
			this.label13.Text = "label13";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.label7, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.textBox10, 1, 5);
			this.tableLayoutPanel1.Controls.Add(this.label12, 0, 5);
			this.tableLayoutPanel1.Controls.Add(this.comboBox1, 1, 0);
			this.tableLayoutPanel1.Controls.Add(this.label8, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label9, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.label10, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.label11, 0, 4);
			this.tableLayoutPanel1.Controls.Add(this.textBox7, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.textBox8, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.textBox9, 1, 4);
			this.tableLayoutPanel1.Controls.Add(this.button5, 1, 1);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 6;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(326, 176);
			this.tableLayoutPanel1.TabIndex = 15;
			// 
			// label7
			// 
			this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label7.Location = new System.Drawing.Point(3, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(157, 44);
			this.label7.TabIndex = 5;
			this.label7.Text = "label7";
			// 
			// textBox10
			// 
			this.textBox10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox10.Location = new System.Drawing.Point(166, 151);
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new System.Drawing.Size(157, 20);
			this.textBox10.TabIndex = 10;
			// 
			// label12
			// 
			this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label12.Location = new System.Drawing.Point(3, 148);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(157, 28);
			this.label12.TabIndex = 14;
			this.label12.Text = "label12";
			// 
			// comboBox1
			// 
			this.comboBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new System.Drawing.Point(166, 3);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(157, 21);
			this.comboBox1.TabIndex = 4;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1SelectedIndexChanged);
			this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.ComboBox1SelectionChangeCommitted);
			// 
			// label8
			// 
			this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label8.Location = new System.Drawing.Point(3, 44);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(157, 26);
			this.label8.TabIndex = 6;
			this.label8.Text = "label8";
			// 
			// label9
			// 
			this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label9.Location = new System.Drawing.Point(3, 70);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(157, 26);
			this.label9.TabIndex = 11;
			this.label9.Text = "label9";
			// 
			// label10
			// 
			this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label10.Location = new System.Drawing.Point(3, 96);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(157, 26);
			this.label10.TabIndex = 12;
			this.label10.Text = "label10";
			// 
			// label11
			// 
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Location = new System.Drawing.Point(3, 122);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(157, 26);
			this.label11.TabIndex = 13;
			this.label11.Text = "label11";
			// 
			// textBox7
			// 
			this.textBox7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox7.Location = new System.Drawing.Point(166, 73);
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(157, 20);
			this.textBox7.TabIndex = 7;
			// 
			// textBox8
			// 
			this.textBox8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox8.Location = new System.Drawing.Point(166, 99);
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new System.Drawing.Size(157, 20);
			this.textBox8.TabIndex = 8;
			// 
			// textBox9
			// 
			this.textBox9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBox9.Location = new System.Drawing.Point(166, 125);
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new System.Drawing.Size(157, 20);
			this.textBox9.TabIndex = 9;
			// 
			// button5
			// 
			this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.button5.Location = new System.Drawing.Point(166, 47);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(157, 20);
			this.button5.TabIndex = 15;
			this.button5.Text = "button5";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.Button5Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(663, 493);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(74, 32);
			this.button6.TabIndex = 17;
			this.button6.Text = "button6";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.Button6Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(335, 53);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(66, 44);
			this.button4.TabIndex = 3;
			this.button4.Text = "button4";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.Button4Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(335, 3);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(66, 44);
			this.button3.TabIndex = 2;
			this.button3.Text = "button3";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.Button3Click);
			// 
			// listView3
			// 
			this.listView3.Location = new System.Drawing.Point(407, 3);
			this.listView3.Name = "listView3";
			this.listView3.Size = new System.Drawing.Size(326, 164);
			this.listView3.TabIndex = 1;
			this.listView3.UseCompatibleStateImageBehavior = false;
			// 
			// listView2
			// 
			this.listView2.Location = new System.Drawing.Point(3, 5);
			this.listView2.Name = "listView2";
			this.listView2.Size = new System.Drawing.Size(326, 164);
			this.listView2.TabIndex = 0;
			this.listView2.UseCompatibleStateImageBehavior = false;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.flowLayoutPanel3);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Size = new System.Drawing.Size(745, 533);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.Controls.Add(this.groupBox6);
			this.flowLayoutPanel3.Controls.Add(this.listView5);
			this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(745, 533);
			this.flowLayoutPanel3.TabIndex = 3;
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.button10);
			this.groupBox6.Controls.Add(this.textBox12);
			this.groupBox6.Controls.Add(this.label23);
			this.groupBox6.Controls.Add(this.checkBox3);
			this.groupBox6.Controls.Add(this.textBox11);
			this.groupBox6.Controls.Add(this.label18);
			this.groupBox6.Controls.Add(this.label17);
			this.groupBox6.Controls.Add(this.button8);
			this.groupBox6.Controls.Add(this.button7);
			this.groupBox6.Location = new System.Drawing.Point(3, 3);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(173, 522);
			this.groupBox6.TabIndex = 2;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "groupBox6";
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(0, 388);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(125, 43);
			this.button10.TabIndex = 9;
			this.button10.Text = "button10";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.Button10Click);
			// 
			// textBox12
			// 
			this.textBox12.Location = new System.Drawing.Point(4, 362);
			this.textBox12.Multiline = true;
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new System.Drawing.Size(121, 20);
			this.textBox12.TabIndex = 8;
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(4, 303);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(169, 56);
			this.label23.TabIndex = 7;
			this.label23.Text = "label23";
			// 
			// checkBox3
			// 
			this.checkBox3.Location = new System.Drawing.Point(5, 456);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(164, 60);
			this.checkBox3.TabIndex = 6;
			this.checkBox3.Text = "checkBox3";
			this.checkBox3.UseVisualStyleBackColor = true;
			this.checkBox3.CheckedChanged += new System.EventHandler(this.CheckBox3CheckedChanged);
			// 
			// textBox11
			// 
			this.textBox11.Location = new System.Drawing.Point(6, 201);
			this.textBox11.Multiline = true;
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new System.Drawing.Size(121, 20);
			this.textBox11.TabIndex = 5;
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(3, 142);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(169, 56);
			this.label18.TabIndex = 4;
			this.label18.Text = "label18";
			// 
			// label17
			// 
			this.label17.Dock = System.Windows.Forms.DockStyle.Top;
			this.label17.Location = new System.Drawing.Point(3, 16);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(167, 23);
			this.label17.TabIndex = 3;
			this.label17.Text = "label17";
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(0, 227);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(125, 43);
			this.button8.TabIndex = 2;
			this.button8.Text = "button8";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.Button8Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(3, 42);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(122, 41);
			this.button7.TabIndex = 1;
			this.button7.Text = "button7";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.Button7Click);
			// 
			// listView5
			// 
			this.listView5.Location = new System.Drawing.Point(182, 3);
			this.listView5.Name = "listView5";
			this.listView5.Size = new System.Drawing.Size(555, 522);
			this.listView5.TabIndex = 0;
			this.listView5.UseCompatibleStateImageBehavior = false;
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.flowLayoutPanel1);
			this.tabPage5.Location = new System.Drawing.Point(4, 22);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(745, 533);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "tabPage5";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Controls.Add(this.label19);
			this.flowLayoutPanel1.Controls.Add(this.label20);
			this.flowLayoutPanel1.Controls.Add(this.label21);
			this.flowLayoutPanel1.Controls.Add(this.listView6);
			this.flowLayoutPanel1.Controls.Add(this.label22);
			this.flowLayoutPanel1.Controls.Add(this.listView7);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(739, 527);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(3, 0);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(733, 46);
			this.label19.TabIndex = 0;
			this.label19.Text = "label19";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label20
			// 
			this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label20.ForeColor = System.Drawing.Color.Red;
			this.label20.Location = new System.Drawing.Point(3, 46);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(731, 80);
			this.label20.TabIndex = 1;
			this.label20.Text = "label20";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(3, 126);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(733, 46);
			this.label21.TabIndex = 4;
			this.label21.Text = "label21";
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// listView6
			// 
			this.listView6.Location = new System.Drawing.Point(3, 175);
			this.listView6.Name = "listView6";
			this.listView6.Size = new System.Drawing.Size(733, 143);
			this.listView6.TabIndex = 2;
			this.listView6.UseCompatibleStateImageBehavior = false;
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(3, 321);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(733, 46);
			this.label22.TabIndex = 5;
			this.label22.Text = "label22";
			this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// listView7
			// 
			this.listView7.Location = new System.Drawing.Point(3, 370);
			this.listView7.Name = "listView7";
			this.listView7.Size = new System.Drawing.Size(733, 142);
			this.listView7.TabIndex = 3;
			this.listView7.UseCompatibleStateImageBehavior = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(753, 559);
			this.Controls.Add(this.tabControl1);
			this.Name = "MainForm";
			this.Text = "poo-trabajo-integrador-2";
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.flowLayoutPanel2.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.tabPage3.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.tabPage4.ResumeLayout(false);
			this.flowLayoutPanel3.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this.tabPage5.ResumeLayout(false);
			this.flowLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
	}

}
	//}
