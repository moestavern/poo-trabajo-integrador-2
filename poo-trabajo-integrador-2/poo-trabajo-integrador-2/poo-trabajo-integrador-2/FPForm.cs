﻿/*
 * Created by SharpDevelop.
 * User: guille puertas
 * Date: 06/12/2018
 * Time: 04:13 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of FPForm.
	/// </summary>
	public partial class FPForm : Form
	{
		uint cuotas;
		double perc;
		
		public FPForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
			label1.Text = "Cantidad de cuotas";
			label2.Text = "Porcentaje de recargo";
			label3.Text = "Agregar nueva forma de pago";
			button1.Text = "Aceptar";
			
		}
		
		public uint Cuotas{get{return cuotas;}}
		public double Perc{get{return perc;}}
		
		void Button1Click(object sender, EventArgs e)
		{
			if (textBox1.Text!="" && textBox2.Text!="") {
				bool ando = uint.TryParse(textBox1.Text, out cuotas) & double.TryParse(textBox2.Text, out perc);
				if(!ando)
				{
					MessageBox.Show( "Ingreso inválido","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
					textBox1.Text="";
					textBox2.Text="";
				}else{
					this.DialogResult = DialogResult.OK;
					textBox1.Text="";
					textBox2.Text="";
					this.Close();
				}
			}
		}
	}
}
