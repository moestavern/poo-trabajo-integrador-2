﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:02 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of Tarjeta.
	/// </summary>
	public class Tarjeta
	{
		private string nombre;
		private string banco;
		private Dictionary<uint, double> formaPago;//uint cuotas, double perc descuento
		//formaPago.Add(3,10);//3 cuotas 10
		bool beneficio;
		
		//Constructores
		public Tarjeta()
		{
			formaPago = new Dictionary<uint, double>();
			formaPago.Add(1,0);
			beneficio=false;
		}
		public Tarjeta(string nombre, string banco)
		{
			this.nombre = nombre;
			this.banco=banco;
			formaPago = new Dictionary<uint, double>();
			formaPago.Add(1,0);
			beneficio=false;
		}
		
		//Propiedades
		public string Nombre{get{return this.nombre;}}
		public string Banco{get{return this.banco;}}
		//Esto devuelve el diccionario con todas las referencias originales, ojo
		public Dictionary<uint,double> FormaPago{get{return formaPago;}}
		public bool Beneficio{get{return beneficio;}}
		
		//Métodos
		public void AgregarFP(uint cuotas, double perc)
		{
			//veo si parametros estan bien
			if (cuotas>0 || cuotas < 24 || perc >=0 || perc<=100) {
				//veo si la clave ya existe
				if (formaPago.ContainsKey(cuotas)) {
					//si existe la borro
					formaPago.Remove(cuotas);
				}
				//agrego clave valor
				formaPago.Add(cuotas, perc);//Si existe la clave devuelve excepción
			}
			else
				throw new ArgumentOutOfRangeException();
		}
		public bool QuitarFP(uint cuotas)
		{
			bool success;
			success = formaPago.Remove(cuotas); //si la clave no existe devuelve false
			return success;
		}
		public void AgregarBeneficio(uint cuotas, double perc)
		{
			this.AgregarFP(cuotas,perc);
			beneficio=true;
		}
	}
}
