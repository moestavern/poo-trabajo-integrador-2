﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:06 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of ListadoProductos.
	/// </summary>
	public class ListadoProductos:Listado<Producto>
	{
		public ListadoProductos():base()
		{
		}
		
		//Métodos
//		public void AgregarPromo(string tipo, string marca, string talle, double promo)
		public void AgregarPromo(Producto elProducto, double promo)
		{
			Producto p=lista.Find(x => x.Tipo == elProducto.Tipo && x.Marca == elProducto.Marca && x.Talle == elProducto.Talle);
			p.Promo=promo;
			/*int indice = lista.IndexOf(elProducto);
			if (indice!=-1) {
				lista[indice].Promo=promo;
			}
			else
				throw new Exception("no se encontró el producto");*/
		}
		// Este método sobra, se puede usar el agregar promo con una promo 0
		/*public void QuitarPromo(string tipo, string marca, string talle)
		{
			Producto elProducto=lista.Find(x => x.Tipo==tipo || x.Marca==marca || x.Talle == talle);
			if (elProducto!=null) {
				elProducto.Promo=0;
			}
			else
				throw new Exception("no se encontró el producto");
		}*/
		public List<Producto> ListarPromo()
		{
			List<Producto> listaPromo;
			return listaPromo = lista.FindAll(x => x.Promo>0);
		}
	}
}
