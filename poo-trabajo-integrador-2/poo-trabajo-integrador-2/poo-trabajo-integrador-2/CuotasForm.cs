﻿/*
 * Created by SharpDevelop.
 * User: guille puertas
 * Date: 06/12/2018
 * Time: 02:49 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of CuotasForm.
	/// </summary>
	public partial class CuotasForm : Form
	{
		string nombre;
		string banco;
		int fp;
		
		public CuotasForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			label1.Text = "Nombre:";
			label2.Text = "Banco:";
			label3.Text = "Formas de pago:";
			AcceptButton=button1;
			CancelButton=button2;
			button1.Text = "Agregar";
			button2.Text= "Cancelar";
		}
		
		public string Nombre{get{return nombre;}}
		public string Banco{get{return banco;}}
		public int Fp{get{return fp;}}
		
		void Button1Click(object sender, EventArgs e)
		{
			if (textBox1.Text!="" && textBox2.Text!="" && textBox3.Text!="") {
				nombre = textBox1.Text;
				banco = textBox2.Text;
				bool ando = int.TryParse(textBox3.Text, out fp);
				if(!ando)
				{
					MessageBox.Show( "Cantidad de formas de pago inválida\r\n(que sonido frustrante, no?)","Danger!",MessageBoxButtons.OK,MessageBoxIcon.Error);
					textBox3.Text="";
				}else{
					this.DialogResult = DialogResult.OK;
					textBox1.Text="";
					textBox2.Text="";
					textBox3.Text="";
					this.Close();
				}
			}
		}

	}
}
