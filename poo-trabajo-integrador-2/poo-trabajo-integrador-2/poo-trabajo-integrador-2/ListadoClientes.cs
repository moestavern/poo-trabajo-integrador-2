﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:06 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of ListadoClientes.
	/// </summary>
	public class ListadoClientes:Listado<Cliente>
	{
		public ListadoClientes()
		{
		}
		
		public Cliente IdentificarCliente(uint dni)
		{
			return lista.Find(x => x.Dni==dni);
		}
		
		public bool ExisteCliente(uint dni)
		{
			bool existe;
			existe=lista.Exists(x => x.Dni == dni);
			return existe;
		}
	}
}
