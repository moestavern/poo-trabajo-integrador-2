﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:04 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of Listado.
	/// </summary>
	public class Listado<T>
	{
		protected List<T> lista;
			
		//Bob el Constructor			
		public Listado()
		{
			lista = new List<T>();
		}
		
		//Métodos
		public void Agregar(T cosa)
		{
			lista.Add(cosa);
		}
		public bool Quitar(T cosa)
		{
			bool success;
			success=lista.Remove(cosa);
			return success;
		}
		public List<T> Listar()
		{
			return lista;
		}
	}
}
