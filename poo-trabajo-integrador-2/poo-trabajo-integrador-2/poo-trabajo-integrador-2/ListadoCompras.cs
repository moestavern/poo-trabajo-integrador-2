﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:07 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of ListadoCompras.
	/// </summary>
	public class ListadoCompras:Listado<Compra>
	{
		public ListadoCompras()
		{
		}
		
		public double CalcularTotalCompras()
		{
			double total=0;
			foreach (Compra c in lista) {
				total = total+c.CalcularPrecioConInteres();
			}
			return total;
		}
	}
}
