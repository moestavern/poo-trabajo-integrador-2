﻿/*
 * Created by SharpDevelop.
 * User: martin
 * Date: 18/10/2018
 * Time: 12:02 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace poo_trabajo_integrador_2
{
	/// <summary>
	/// Description of Producto.
	/// </summary>
	public class Producto
	{
		private string tipo;
		private string marca;
		private string talle;
		private double precio;
		private double promo;
		
		// Constructores
		public Producto()
		{
		}
		
		public Producto(string tipo, string marca, string talle, double precio, double promo)
		{
			this.tipo=tipo;
			this.marca=marca;
			this.talle=talle;
			this.precio=precio;
			this.promo=promo;
		}
		
		//Propiedades
		public string Tipo{get{return this.tipo;}}
		public string Marca{get{return this.marca;}}
		public string Talle{get{return this.talle;}}
		public double Precio{get{return this.precio;}}
		public double Promo{
			get{return this.promo;}
			set{if (value>=0 && value<=100) {
					promo=value;
				}else
					throw new ArgumentOutOfRangeException();
			}
		}
	}
}
